﻿Public Class graphing
    Dim xMin As Decimal = -10
    Dim xMax As Decimal = +10
    Dim yMin As Decimal = -10
    Dim yMax As Decimal = +10

    Dim Plot As String

    Public Sub SetPlot()
        If RadioButton1.Checked Then
            Plot = TextBox1.Text
        ElseIf RadioButton2.Checked Then
            Plot = TextBox2.Text
        ElseIf RadioButton3.Checked Then
            Plot = TextBox3.Text
        Else
            Plot = TextBox4.Text
        End If
    End Sub

    Public Function NthDerivative(ByVal Expression As String, ByVal X As Decimal, ByVal N As Integer) As Decimal
        Dim dX As Decimal = 0.1
        Dim Derivative As Decimal = 0
        Dim PrevDerivative As Decimal = 1
        While Math.Round(Derivative, 6) <> Math.Round(PrevDerivative, 6)
            PrevDerivative = Derivative
            Console.WriteLine(Derivative)
            If N = 1 Then
                Derivative = (mathexpression.evaluate(Expression, False, 0, X + dX) - mathexpression.evaluate(Expression, False, 0, X)) / (dX)
            Else
                Derivative = (NthDerivative(Expression, X + dX, N - 1) - NthDerivative(Expression, X, N - 1)) / (dX)
            End If
            dX /= 10
            If dX < 0.001 Then Exit While
        End While
        Return Derivative
    End Function

    Private Sub graphing_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'draw X-axis and Y-axis 
        Chart1.ChartAreas(0).AxisX.Minimum = xMin
        Chart1.ChartAreas(0).AxisY.Minimum = yMin
        Chart1.ChartAreas(0).AxisX.Maximum = xMax
        Chart1.ChartAreas(0).AxisY.Maximum = yMax
        Chart1.Series("xAxis").Points.AddXY(xMin, 0)
        Chart1.Series("xAxis").Points.AddXY(xMax, 0)
        Chart1.Series("yAxis").Points.AddXY(0, yMin)
        Chart1.Series("yAxis").Points.AddXY(0, yMax)
    End Sub

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        Chart1.Series(0).Points.Clear()
        For x As Decimal = xMin To xMax Step (xMax - xMin) / 100
            Try
                Chart1.Series(0).Points.AddXY(x, mathexpression.evaluate(TextBox1.Text, False, 0, x))
            Catch
                Console.WriteLine("Invalid for x = " & x.ToString)
            End Try
        Next
    End Sub

    Private Sub Button6_Click_1(sender As Object, e As EventArgs) Handles Button6.Click
        Chart1.Series(1).Points.Clear()
        For x As Decimal = xMin To xMax Step (xMax - xMin) / 100
            Try
                Chart1.Series(1).Points.AddXY(x, mathexpression.evaluate(TextBox2.Text, False, 0, x))
            Catch
                Console.WriteLine("Invalid for x = " & x.ToString)
            End Try
        Next
    End Sub

    Private Sub Button7_Click_1(sender As Object, e As EventArgs) Handles Button7.Click
        Chart1.Series(2).Points.Clear()
        For x As Decimal = xMin To xMax Step (xMax - xMin) / 100
            Try
                Chart1.Series(2).Points.AddXY(x, mathexpression.evaluate(TextBox3.Text, False, 0, x))
            Catch
                Console.WriteLine("Invalid for x = " & x.ToString)
            End Try
        Next
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Chart1.Series(3).Points.Clear()
        For x As Decimal = xMin To xMax Step (xMax - xMin) / 100
            Try
                Chart1.Series(3).Points.AddXY(x, mathexpression.evaluate(TextBox4.Text, False, 0, x))
            Catch
                Console.WriteLine("Invalid for x = " & x.ToString)
            End Try
        Next
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Clear()
        Chart1.Series(0).Points.Clear()
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox2.Clear()
        Chart1.Series(1).Points.Clear()
    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        TextBox3.Clear()
        Chart1.Series(2).Points.Clear()
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        TextBox4.Clear()
        Chart1.Series(3).Points.Clear()
    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.TextChanged
        Try
            xMin = CDec(TextBox5.Text)
            Chart1.ChartAreas(0).AxisX.Minimum = xMin
            Chart1.Series("xAxis").Points.Clear()
            Chart1.Series("xAxis").Points.AddXY(xMin, 0)
            Chart1.Series("xAxis").Points.AddXY(xMax, 0)
        Catch ex As Exception
            Console.Write(ex)
        End Try
    End Sub

    Private Sub TextBox6_TextChanged(sender As Object, e As EventArgs) Handles TextBox6.TextChanged
        Try
            yMin = CDec(TextBox6.Text)
            Chart1.ChartAreas(0).AxisY.Minimum = yMin
            Chart1.Series("yAxis").Points.Clear()
            Chart1.Series("yAxis").Points.AddXY(0, yMin)
            Chart1.Series("yAxis").Points.AddXY(0, yMax)
        Catch ex As Exception
            Console.Write(ex)
        End Try
    End Sub

    Private Sub TextBox7_TextChanged(sender As Object, e As EventArgs) Handles TextBox7.TextChanged
        Try
            xMax = CDec(TextBox7.Text)
            Chart1.ChartAreas(0).AxisX.Maximum = xMax
            Chart1.Series("xAxis").Points.Clear()
            Chart1.Series("xAxis").Points.AddXY(xMin, 0)
            Chart1.Series("xAxis").Points.AddXY(xMax, 0)
        Catch ex As Exception
            Console.Write(ex)
        End Try
    End Sub

    Private Sub TextBox8_TextChanged(sender As Object, e As EventArgs) Handles TextBox8.TextChanged
        Try
            yMax = CDec(TextBox8.Text)
            Chart1.ChartAreas(0).AxisY.Maximum = yMax
            Chart1.Series("yAxis").Points.Clear()
            Chart1.Series("yAxis").Points.AddXY(0, yMin)
            Chart1.Series("yAxis").Points.AddXY(0, yMax)
        Catch ex As Exception
            Console.Write(ex)
        End Try
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        SetPlot()
        'Slope
        Try
            Dim x1 As Decimal = CDec(TextBox10.Text)
            Dim y1 As Decimal = mathexpression.evaluate(Plot, False, 0, x1)
            Dim slope As Decimal = Math.Round(NthDerivative(Plot, x1, 1), 10)
            TextBox9.Text = slope
            Chart1.Series("Slope").Points.Clear()
            Dim c As Decimal = y1 - slope * x1
            Chart1.Series("Slope").Points.AddXY(x1, y1)
            Chart1.Series("Slope").Points.AddXY(xMin, slope * xMin + c)
            Chart1.Series("Slope").Points.AddXY(xMax, slope * xMax + c)
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        SetPlot()
        'Finding Local Minima
        Dim precision As Decimal = (xMax - xMin) / 30
        Dim x1 As Decimal = xMin
        Dim slope As Decimal
        'keep moving with precision till the negative slope is found
        While x1 < xMax
            x1 += precision
            Try
                slope = NthDerivative(Plot, x1, 1)
                If slope < 0 Then Exit While
            Catch ex As Exception
                Console.Write("moving further")
            End Try
        End While
        'now keep moving till positive slope is found
        While x1 < xMax
            While x1 < xMax
                Try
                    slope = NthDerivative(Plot, x1, 1)
                    If slope > 0 Then
                        x1 -= precision
                        Exit While
                    End If
                Catch ex As Exception
                    TextBox11.Text = "Not found"
                    Exit Sub
                End Try
                x1 += precision
            End While
            If Math.Round(slope, 5) = 0 Then
                TextBox11.Text = Math.Round(x1, 8)
                Exit Sub
            End If
            precision /= 10
        End While
        TextBox11.Text = "Not found"
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        SetPlot()
        'Finding Local Maxima
        Dim precision As Decimal = (xMax - xMin) / 30
        Dim x1 As Decimal = xMin
        Dim slope As Decimal
        'keep moving with precision till the positive slope is found
        While x1 < xMax
            x1 += precision
            Try
                slope = NthDerivative(Plot, x1, 1)
                If slope > 0 Then Exit While
            Catch ex As Exception
                Console.Write("moving further")
            End Try
        End While
        'now keep moving till negative slope is found
        While x1 < xMax
            While x1 < xMax
                Try
                    slope = NthDerivative(Plot, x1, 1)
                    If slope < 0 Then
                        x1 -= precision
                        Exit While
                    End If
                Catch ex As Exception
                    TextBox12.Text = "Not found"
                    Exit Sub
                End Try
                x1 += precision
            End While
            If Math.Round(slope, 5) = 0 Then
                TextBox12.Text = Math.Round(x1, 8)
                Exit Sub
            End If
            precision /= 10
        End While
        TextBox12.Text = "Not found"
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        SetPlot()
        'Find the nth derivative
        TextBox17.Text = Math.Round(NthDerivative(Plot, CDec(TextBox13.Text), ComboBox2.SelectedIndex + 1), 8)
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        SetPlot()
        'Find Definite Integral from textbox 14 to 15 and write it in 16
        'using the trapezoidal rule
        Dim Start As Decimal = CDec(TextBox14.Text)
        Dim Upto As Decimal = CDec(TextBox15.Text)
        Dim Sum As Decimal = mathexpression.evaluate(Plot, False, 0, Start) + mathexpression.evaluate(Plot, False, 0, Upto)
        Dim Partitions As Integer = 300
        Dim dX As Decimal = (Start - Upto) / Partitions
        For i As Integer = 1 To (Partitions - 1) Step 1
            Sum += 2 * mathexpression.evaluate(Plot, False, 0, Start + i * dX)
        Next
        TextBox16.Text = (dX / 2) * Sum
        'now add the area inside the graph
        Chart1.Series("Area").Points.Clear()
        For j As Decimal = Start To Upto Step 0.05
            Chart1.Series("Area").Points.AddXY(j, mathexpression.evaluate(Plot, False, 0, j))
        Next
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        'Finding Intersection

        Dim Plot2 As String
        If ComboBox1.SelectedIndex = 0 Then
            Plot2 = TextBox1.Text
        ElseIf ComboBox1.SelectedIndex = 1 Then
            Plot2 = TextBox2.Text
        ElseIf ComboBox1.SelectedIndex = 2 Then
            Plot2 = TextBox3.Text
        Else
            Plot2 = TextBox4.Text
        End If
        'thus find root for plot1 - plot2, using newton raphson method =>
        Chart1.Series("Intersection").Points.Clear()
        Dim differencePlot As String = Plot & "−(" & Plot2 & ")"
        For i As Decimal = xMin To xMax Step 1
            Try
                If mathexpression.evaluate(differencePlot, False, 0, i) < 0.5 Then
                    'find the exact root here and point it on graph
                    Dim x As Decimal = i
                    Dim h As Decimal = mathexpression.evaluate(differencePlot, False, 0, x) / NthDerivative(differencePlot, x, 1)
                    While Math.Round(h, 5) <> 0
                        x -= h
                        h = mathexpression.evaluate(differencePlot, False, 0, x) / NthDerivative(differencePlot, x, 1)
                    End While
                    Chart1.Series("Intersection").Points.AddXY(x, mathexpression.evaluate(Plot, False, 0, x))
                End If
            Catch ex As Exception
                Console.Write("no values as x =" & i.ToString())
            End Try
        Next
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Button12.Enabled = True
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter
        'clear the slope series.
        Chart1.Series("Slope").Points.Clear()
        'clear the intersection series.
        Chart1.Series("Intersection").Points.Clear()
        'clear the integral series.
        Chart1.Series("Area").Points.Clear()
    End Sub

    Private Sub TextBox13_TextChanged(sender As Object, e As EventArgs) Handles TextBox13.TextChanged
        ComboBox2.Enabled = True
    End Sub

    Private Sub TextBox14_TextChanged(sender As Object, e As EventArgs) Handles TextBox14.TextChanged
        Button13.Enabled = True
    End Sub
End Class