﻿Public Class scientific
    Dim expression As String = ""
    Dim index As Integer = 0
    Dim a As Decimal = 0
    Dim b As Decimal = 0
    Dim c As Decimal = 0
    Dim Ans As Decimal = 0

    Private Sub expression_box_Click(sender As Object, e As EventArgs) Handles expression_box.Click
        index = expression_box.SelectionStart
        Console.WriteLine(index)
    End Sub

    Private Sub key7_Click(sender As Object, e As EventArgs) Handles key7.Click
        expression = expression.Insert(index, "7")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key8_Click(sender As Object, e As EventArgs) Handles key8.Click
        expression = expression.Insert(index, "8")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key9_Click(sender As Object, e As EventArgs) Handles key9.Click
        expression = expression.Insert(index, "9")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key4_Click(sender As Object, e As EventArgs) Handles key4.Click
        expression = expression.Insert(index, "4")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key5_Click(sender As Object, e As EventArgs) Handles key5.Click
        expression = expression.Insert(index, "5")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key6_Click(sender As Object, e As EventArgs) Handles key6.Click
        expression = expression.Insert(index, "6")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key1_Click(sender As Object, e As EventArgs) Handles key1.Click
        expression = expression.Insert(index, "1")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key2_Click(sender As Object, e As EventArgs) Handles key2.Click
        expression = expression.Insert(index, "2")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key3_Click(sender As Object, e As EventArgs) Handles key3.Click
        expression = expression.Insert(index, "3")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub key0_Click(sender As Object, e As EventArgs) Handles key0.Click
        expression = expression.Insert(index, "0")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub decimalpoint_Click(sender As Object, e As EventArgs) Handles decimalpoint.Click
        expression = expression.Insert(index, ".")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub divbutton_Click(sender As Object, e As EventArgs) Handles divbutton.Click
        expression = expression.Insert(index, "÷")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub multbutton_Click(sender As Object, e As EventArgs) Handles multbutton.Click
        expression = expression.Insert(index, "×")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub addbutton_Click(sender As Object, e As EventArgs) Handles addbutton.Click
        expression = expression.Insert(index, "+")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub subbutton_Click(sender As Object, e As EventArgs) Handles subbutton.Click
        expression = expression.Insert(index, "−")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub leftpar_Click(sender As Object, e As EventArgs) Handles leftpar.Click
        expression = expression.Insert(index, "(")
        Console.WriteLine("(")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub rightpar_Click(sender As Object, e As EventArgs) Handles rightpar.Click
        expression = expression.Insert(index, ")")
        Console.WriteLine(")")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub powbutton_Click(sender As Object, e As EventArgs) Handles powbutton.Click
        expression = expression.Insert(index, "^")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub equalbutton_Click(sender As Object, e As EventArgs) Handles equalbutton.Click
        Try
            answerbox.Text = Math.Round(mathexpression.evaluate(expression, RadioButton1.Checked, Ans, a, b, c), 7)
            Ans = answerbox.Text
        Catch ex As Exception
            MessageBox.Show("Math Expression entered seems incorrect. Please validate.", "Invalid Expression", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub delbutton_Click(sender As Object, e As EventArgs) Handles delbutton.Click
        Try
            expression = expression.Remove(index - 1, 1)
        Catch ex As Exception
            Console.WriteLine("cursor at the beginning")
        End Try
        index = index - 1
        expression_box.Text = expression
    End Sub

    Private Sub clearbutton_Click(sender As Object, e As EventArgs) Handles clearbutton.Click
        expression = ""
        index = 0
        expression_box.Text = expression
        answerbox.Text = ""
    End Sub

    Private Sub store_a_Click(sender As Object, e As EventArgs) Handles store_a.Click
        Try
            a = CDec(Val(answerbox.Text))
        Catch ex As Exception
            MessageBox.Show("No valid Answer obtained to be stored at A")
        End Try
    End Sub

    Private Sub store_b_Click(sender As Object, e As EventArgs) Handles store_b.Click
        Try
            b = CDec(Val(answerbox.Text))
        Catch ex As Exception
            MessageBox.Show("No valid Answer obtained to be stored at A")
        End Try
    End Sub

    Private Sub store_c_Click(sender As Object, e As EventArgs) Handles store_c.Click
        Try
            c = CDec(Val(answerbox.Text))
        Catch ex As Exception
            MessageBox.Show("No valid Answer obtained to be stored at A")
        End Try
    End Sub

    Private Sub load_a_Click(sender As Object, e As EventArgs) Handles load_a.Click
        expression = expression.Insert(index, "X")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub load_b_Click(sender As Object, e As EventArgs) Handles load_b.Click
        expression = expression.Insert(index, "Y")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub load_c_Click(sender As Object, e As EventArgs) Handles load_c.Click
        expression = expression.Insert(index, "Z")
        index = index + 1
        expression_box.Text = expression
    End Sub

    Private Sub square_Click(sender As Object, e As EventArgs) Handles square.Click
        Dim strexp As String
        strexp = "^2"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub cube_Click(sender As Object, e As EventArgs) Handles cube.Click
        Dim strexp As String
        strexp = "^3"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub inverse_Click(sender As Object, e As EventArgs) Handles inverse.Click
        Dim strexp As String
        strexp = "^(−1)"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub squareroot_Click(sender As Object, e As EventArgs) Handles squareroot.Click
        Dim strexp As String
        strexp = "√("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub cuberoot_Click(sender As Object, e As EventArgs) Handles cuberoot.Click
        Dim strexp As String
        strexp = "³√("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub answer_Click(sender As Object, e As EventArgs) Handles answer.Click
        Dim strexp As String
        strexp = "%"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim strexp As String
        strexp = "sin("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim strexp As String
        strexp = "cos("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim strexp As String
        strexp = "tan("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim strexp As String
        strexp = "log("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        Dim strexp As String
        strexp = "logₑ("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click
        Dim strexp As String
        strexp = "cosh("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Dim strexp As String
        strexp = "tan ̄ ¹("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim strexp As String
        strexp = "e"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        Dim strexp As String
        strexp = "sinh("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click
        Dim strexp As String
        strexp = "tanh("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click
        Dim strexp As String
        strexp = "×10^("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim strexp As String
        strexp = "×e^("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click
        Dim strexp As String
        strexp = "π"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Dim strexp As String
        strexp = "sin ̄ ¹("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim strexp As String
        strexp = "cos ̄ ¹("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Dim strexp As String
        strexp = "!"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Dim strexp As String
        strexp = "sinh ̄ ¹("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Dim strexp As String
        strexp = "cosh ̄ ¹("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim strexp As String
        strexp = "tanh ̄ ¹("
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim strexp As String
        strexp = "Ans"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub scientific_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Size = New System.Drawing.Size(1397, 648)
        Me.CenterToScreen()
    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        Dim strexp As String
        strexp = "C"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Dim strexp As String
        strexp = "P"
        expression = expression.Insert(index, strexp)
        index = index + strexp.Length
        expression_box.Text = expression
    End Sub
End Class