﻿Public Class mathexpression
    Public Shared precedencestr As String = "(+-−*×/÷%CP^" 'note : not the hyphen as minus

    Public Shared Function operate(ByVal oprand As String, ByVal x As Decimal, ByVal y As Decimal) As Decimal
        If oprand = "^" Then
            Console.WriteLine(x.ToString() & "to the power" & y.ToString())
            Return (Math.Pow(x, y))
        ElseIf oprand = "÷" Or oprand = "/" Then
            Console.WriteLine("division of " & x.ToString() & " with " & y.ToString())
            Return (x / y)
        ElseIf oprand = "×" Or oprand = "*" Then
            Console.WriteLine("multiplication of " & x.ToString() & " with " & y.ToString())
            Return (x * y)
        ElseIf oprand = "−" Or oprand = "-" Then
            Console.WriteLine("subtraction of " & x.ToString() & " from " & y.ToString())
            Return (x - y)
        ElseIf oprand = "+" Then
            Console.WriteLine("addition of " & x.ToString() & " with " & y.ToString())
            Return (x + y)
        ElseIf oprand = "%" Then
            Console.WriteLine("modulus of " & x.ToString() & " from " & y.ToString())
            Return (x Mod y)
        ElseIf oprand = "C" Then
            Console.WriteLine("combination of " & x.ToString() & " in " & y.ToString())
            Return Combination(x, y)
        ElseIf oprand = "P" Then
            Console.WriteLine("permutation of " & x.ToString() & " in " & y.ToString())
            Return Permutation(x, y)
        Else
            Console.WriteLine("exception occured in operate function")
            Throw New Exception("Cannot understand the oprand")
        End If
    End Function

    Shared Function Combination(n, r) As Decimal 'The return type has to be decimal
        If n < 1 Or r < 1 Then
            Return 0
        Else
            If r > n Then
                Return 0
            Else
                If r = n Then
                    Return 1
                Else
                    Return (factorial(n) / (factorial(n - r) * factorial(r)))
                End If
            End If
        End If
    End Function

    Shared Function Permutation(n, r) As Decimal 'The return type has to be decimal
        If n < 1 Or r < 1 Then
            Return 0
        Else
            If r > n Then
                Return 0
            Else
                Return (factorial(n) / factorial(n - r))
            End If
        End If
    End Function

    Shared Function factorial(ByVal n As Integer) As Int64
        If n = 0 Or n = 1 Then
            Return 1
        Else
            Return n * factorial(n - 1)
        End If
    End Function

    Public Shared Function evaluate(ByVal expression As String, ByVal degree As Boolean, Ans As Decimal, Optional ByVal a As Decimal = 0, Optional ByVal b As Decimal = 0, Optional ByVal c As Decimal = 0) As Decimal
        Dim numstack As Stack = New Stack()
        Dim opstack As Stack = New Stack()
        Dim i As Integer = 0
        Console.WriteLine(expression)
        While i < expression.Length
            Console.WriteLine("evaluating " & expression(i))

            'numeric
            If IsNumeric(expression(i)) Or expression(i) = "." Then 'a numeric character is encoutered here
                Console.WriteLine("numeric expression obtained")
                Dim length As Integer = 1
                While i + length < expression.Length
                    If IsNumeric(expression(i + length)) Or expression(i + length) = "." Then
                        length = length + 1
                    Else
                        Exit While
                    End If
                End While
                Console.WriteLine("the length of number is " & length.ToString())
                Try
                    numstack.Push(CDec(Val(expression.Substring(i, length))))
                    i = i + length
                Catch ex As Exception
                    MessageBox.Show("Invalid Expression Inserted")
                    Console.WriteLine(ex.ToString())
                End Try

                'variables
            ElseIf expression(i) = "X" Then
                numstack.Push(a)
                i = i + 1

            ElseIf expression(i) = "Y" Then
                numstack.Push(b)
                i = i + 1

            ElseIf expression(i) = "Z" Then
                numstack.Push(c)
                i = i + 1

            ElseIf expression(i) = "e" Then
                numstack.Push(Math.E)
                i = i + 1

            ElseIf expression(i) = "π" Then
                numstack.Push(Math.PI)
                i = i + 1

            ElseIf expression(i) = "A" Then
                numstack.Push(Ans)
                i = i + 3

                'signs --> + or -
            ElseIf (expression(i) = "−" Or expression(i) = "+") And i = 0 Then 'special case where minus is unary
                numstack.Push(0)
                opstack.Push(expression(i))
                i = i + 1

                'Open Paranthesis --> (
            ElseIf expression(i) = "(" Then
                If i <> 0 Then
                    If IsNumeric(expression(i - 1)) Then
                        opstack.Push("×")
                    End If
                End If
                opstack.Push("(")
                If expression(i + 1) = "−" Or expression(i + 1) = "+" Then 'special case where minus is unary
                    numstack.Push(0)
                    opstack.Push(expression(i + 1))
                    i = i + 1
                End If
                i = i + 1

                'Closed Paranthesis --> )
            ElseIf expression(i) = ")" Then 'pop till ( is popped
                Dim lastop As String = opstack.Pop()
                While lastop <> "("
                    Dim lastnum As Decimal = numstack.Pop()
                    Dim seclastnum As Decimal = numstack.Pop()
                    numstack.Push(mathexpression.operate(lastop, seclastnum, lastnum))
                    lastop = opstack.Pop()
                End While
                i = i + 1

                'factorial ---> !
            ElseIf expression(i) = "!" Then
                Console.WriteLine("! encountered")
                If IsNumeric(expression(i - 1)) Then
                    Dim X As Decimal = numstack.Pop()
                    numstack.Push(factorial(X))
                Else
                    Throw New Exception("factorial error")
                End If
                i += 1

                'square root --> √(
            ElseIf expression(i) = "√" Then
                Dim ExpressionStart As Integer = i + 2
                Dim openbracket As Integer = 1
                i = i + 2
                While openbracket <> 0
                    If expression(i) = "(" Then
                        openbracket += 1
                    ElseIf expression(i) = ")" Then
                        openbracket -= 1
                    End If
                    i = i + 1
                End While
                Dim Expressionlen As Integer = i - ExpressionStart - 1
                numstack.Push(Math.Sqrt(evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)))

            ElseIf expression(i) = "³" Then
                Dim ExpressionStart As Integer = i + 3
                Dim openbracket As Integer = 1
                i = ExpressionStart
                While openbracket <> 0
                    If expression(i) = "(" Then
                        openbracket += 1
                    ElseIf expression(i) = ")" Then
                        openbracket -= 1
                    End If
                    i = i + 1
                End While
                Dim Expressionlen As Integer = i - ExpressionStart - 1
                Dim ExpressionTemp As String = "(" & expression.Substring(ExpressionStart, Expressionlen) & ")^(1÷3)"
                numstack.Push(evaluate(ExpressionTemp, degree, Ans, a, b, c))

                'for sin , sinh, sin-1, sinh-1
            ElseIf expression(i) = "s" Then
                If expression.Substring(i, "sin ̄ ¹(".Length) = "sin ̄ ¹(" Then 'sin -1
                    Dim ExpressionStart As Integer = i + "sin ̄ ¹(".Length
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    numstack.Push(Math.Asin(X))
                ElseIf expression(i + 3) = "h" Then 'sinh and sinh -1
                    If expression.Substring(i, "sinh ̄ ¹(".Length) = "sinh ̄ ¹(" Then 'sinh -1(
                        Dim ExpressionStart As Integer = i + "sinh ̄ ¹(".Length
                        Dim openbracket As Integer = 1
                        i = ExpressionStart
                        While openbracket <> 0
                            If expression(i) = "(" Then
                                openbracket += 1
                            ElseIf expression(i) = ")" Then
                                openbracket -= 1
                            End If
                            i = i + 1
                        End While
                        Dim Expressionlen As Integer = i - ExpressionStart - 1
                        Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                        numstack.Push(Math.Log(X + Math.Sqrt(X * X + 1))) 'Asinh
                    Else   'sinh
                        Dim ExpressionStart As Integer = i + 5
                        Dim openbracket As Integer = 1
                        i = ExpressionStart
                        While openbracket <> 0
                            If expression(i) = "(" Then
                                openbracket += 1
                            ElseIf expression(i) = ")" Then
                                openbracket -= 1
                            End If
                            i = i + 1
                        End While
                        Dim Expressionlen As Integer = i - ExpressionStart - 1
                        Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                        If degree Then
                            'convert it into radian
                            X = X * (Math.PI) / 180
                        End If
                        numstack.Push(Math.Sinh(X)) 'sinh
                    End If
                Else 'sin
                    Dim ExpressionStart As Integer = i + 4
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    If degree Then
                        'convert it into radian
                        X = X * (Math.PI) / 180
                    End If
                    numstack.Push(Math.Sin(X)) 'sin
                End If



                'for cos , cosh, cos-1, cosh-1
            ElseIf expression(i) = "c" Then
                If expression.Substring(i, "cos ̄ ¹(".Length) = "cos ̄ ¹(" Then 'cos -1
                    Dim ExpressionStart As Integer = i + "cos ̄ ¹(".Length
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    numstack.Push(Math.Acos(X))

                ElseIf expression(i + 3) = "h" Then 'cosh and cosh -1
                    If expression.Substring(i, "cosh ̄ ¹(".Length) = "cosh ̄ ¹(" Then 'cosh -1(
                        Dim ExpressionStart As Integer = i + "cosh ̄ ¹(".Length
                        Dim openbracket As Integer = 1
                        i = ExpressionStart
                        While openbracket <> 0
                            If expression(i) = "(" Then
                                openbracket += 1
                            ElseIf expression(i) = ")" Then
                                openbracket -= 1
                            End If
                            i = i + 1
                        End While
                        Dim Expressionlen As Integer = i - ExpressionStart - 1
                        Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                        numstack.Push(Math.Log(X + Math.Sqrt(X * X - 1))) 'Acosh
                    Else   'cosh
                        Dim ExpressionStart As Integer = i + 5
                        Dim openbracket As Integer = 1
                        i = ExpressionStart
                        While openbracket <> 0
                            If expression(i) = "(" Then
                                openbracket += 1
                            ElseIf expression(i) = ")" Then
                                openbracket -= 1
                            End If
                            i = i + 1
                        End While
                        Dim Expressionlen As Integer = i - ExpressionStart - 1
                        Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                        If degree Then
                            'convert it into radian
                            X = X * (Math.PI) / 180
                        End If
                        numstack.Push(Math.Cosh(X)) 'cosh
                    End If

                Else 'cos
                    Dim ExpressionStart As Integer = i + 4
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    If degree Then
                        'convert it into radian
                        X = X * (Math.PI) / 180
                    End If
                    numstack.Push(Math.Cos(X)) 'cos
                End If

                'tan, tanh, tan -1, tanh -1
            ElseIf expression(i) = "t" Then
                If expression.Substring(i, "tan ̄ ¹(".Length) = "tan ̄ ¹(" Then 'tan -1
                    Dim ExpressionStart As Integer = i + "tan ̄ ¹(".Length
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Console.WriteLine(expression.Substring(ExpressionStart, Expressionlen))
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    numstack.Push(Math.Atan(X))

                ElseIf expression(i + 3) = "h" Then 'tanh and tanh -1
                    If expression.Substring(i, "tanh ̄ ¹(".Length) = "tanh ̄ ¹(" Then 'tanh -1(
                        Dim ExpressionStart As Integer = i + "tanh ̄ ¹(".Length
                        Dim openbracket As Integer = 1
                        i = ExpressionStart
                        While openbracket <> 0
                            If expression(i) = "(" Then
                                openbracket += 1
                            ElseIf expression(i) = ")" Then
                                openbracket -= 1
                            End If
                            i = i + 1
                        End While
                        Dim Expressionlen As Integer = i - ExpressionStart - 1
                        Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                        numstack.Push(Math.Log((1 + X) / (1 - X)) / 2) 'Atanh
                    Else   'tanh
                        Dim ExpressionStart As Integer = i + 5
                        Dim openbracket As Integer = 1
                        i = ExpressionStart
                        While openbracket <> 0
                            If expression(i) = "(" Then
                                openbracket += 1
                            ElseIf expression(i) = ")" Then
                                openbracket -= 1
                            End If
                            i = i + 1
                        End While
                        Dim Expressionlen As Integer = i - ExpressionStart - 1
                        Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                        If degree Then
                            'convert it into radian
                            X = X * (Math.PI) / 180
                        End If
                        numstack.Push(Math.Tanh(X)) 'tanh
                    End If

                Else 'tan
                    Dim ExpressionStart As Integer = i + 4
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    If degree Then
                        'convert it into radian
                        X = X * (Math.PI) / 180
                    End If
                    numstack.Push(Math.Tan(X)) 'tan
                End If

            ElseIf expression(i) = "l" Then 'log and loge
                If expression(i + 3) = "(" Then
                    Dim ExpressionStart As Integer = i + 4
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    numstack.Push(Math.Log10(X)) 'log base 10
                Else
                    Dim ExpressionStart As Integer = i + 5
                    Dim openbracket As Integer = 1
                    i = ExpressionStart
                    While openbracket <> 0
                        If expression(i) = "(" Then
                            openbracket += 1
                        ElseIf expression(i) = ")" Then
                            openbracket -= 1
                        End If
                        i = i + 1
                    End While
                    Dim Expressionlen As Integer = i - ExpressionStart - 1
                    Dim X As Decimal = evaluate(expression.Substring(ExpressionStart, Expressionlen), degree, Ans, a, b, c)
                    numstack.Push(Math.Log(X)) 'log base e
                End If

            Else 'cases of ^ / * + - P C %
                If opstack.Count >= 1 Then
                    While precedencestr.IndexOf(expression(i)) <= precedencestr.IndexOf(opstack.Peek())
                        'keep popping till op of lower precedence found
                        Dim lastop As String = opstack.Pop()
                        Dim lastnum As Decimal = numstack.Pop()
                        Dim seclastnum As Decimal = numstack.Pop()
                        numstack.Push(mathexpression.operate(lastop, seclastnum, lastnum))
                        If opstack.Count = 0 Then 'or if stack becomes empty
                            Exit While
                        End If
                    End While
                    opstack.Push(expression(i))
                Else
                    opstack.Push(expression(i))
                End If
                i = i + 1
            End If
        End While

        'Emptying the whole opstack
        While opstack.Count > 0
            Dim lastop As String = opstack.Pop()
            Dim lastnum As Decimal = numstack.Pop()
            Dim seclastnum As Decimal = numstack.Pop()
            numstack.Push(mathexpression.operate(lastop, seclastnum, lastnum))
        End While

        Console.WriteLine("The answer should be " & numstack.Peek().ToString())
        Return numstack.Peek()
    End Function
End Class
